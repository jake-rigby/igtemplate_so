
module.exports = function (gulp, manifest, namespace, debug) {

	console.log(manifest, namespace, debug);


	var browserify = require('browserify'),
		transform = require('vinyl-transform'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
		del = require('del'),
		//concatcss = require('gulp-concat-css'), // <-- don't use concatcss as this will rebase fontawsome urls
		uglifycss = require('gulp-uglifycss'),
		mainBowerFiles = require('main-bower-files'),
		ngAnnotate = require('gulp-ng-annotate'),
		filter = require('gulp-filter'),
		fs = require('fs-extra'),
		// manifest = require('./manifest'),
		replace = require('gulp-replace'),
		runsequence = require('run-sequence'),
		// config = require('./config'),
		flatten = require('flat'),
		rename = require('gulp-rename'),
		sass = require('gulp-sass'),
		jeditor = require('gulp-json-editor'),
		merge = require('merge2'),
		source = require("vinyl-source-stream");
		// reactify = require('reactify');

	gulp.task('browserify', function () {
		var browserified = transform(function (filename) { // http://stackoverflow.com/questions/24329690/how-to-expose-require-to-the-browser-when-using-browserify-from-within-gulp
	 		var b = browserify(filename);
			b.require(filename, {expose:namespace})
			// b.transform(reactify);
	 			// .external('some_modeule_we_promise_is_there') // TODO: get a list of these from the manifestSocketioJwt
			return b.bundle();
		})
		return gulp.src('./browserify.js') // <-- this file should contain all server/client common source files (will NOT work as string path s in the manifest)
			.pipe(browserified)
			// .pipe(uglify())
			.pipe(gulp.dest('browserified'))
	})

	gulp.task('html', function () {
		return gulp.src(manifest.static.html)
			.pipe(gulp.dest('build/www'))
	})

	gulp.task('fonts', function (cb) {
	    return gulp.src(manifest.static.fonts) // <-- nb. hardcode for bootstrap and FA
	        .pipe(gulp.dest('build/www/fonts'));
	})

	gulp.task('images', function () {
		return gulp.src(manifest.static.images)
			.pipe(gulp.dest('build/www/images'));
	})

	gulp.task('images-deps', function () {
		return gulp.src(mainBowerFiles())
			.pipe(filter(['*.png','*.gif','*.jpg']))
			.pipe(gulp.dest('build/www/images'))
	})

	gulp.task('html-deps', function () {
		return gulp.src(mainBowerFiles())
			.pipe(filter('*.html'))
			.pipe(gulp.dest('build/www'));
	})

	// gulp.task('css-deps', function () {
	// 	console.log('css-deps: sourced from mainBowerFiles, special case for fontawesome .css, /scss and bootstrap-sass')
	// 	return merge(
	// 			gulp.src(mainBowerFiles()
	// 				.concat(['bower_components/fontawesome/css/font-awesome.css']) )
	// 				.pipe(filter('*.css')),
	// 			gulp.src(manifest.static.scss) // <-- hack
	// 				.pipe(sass({
	// 					includePaths:['./bower_components/bootstrap-sass/assets/stylesheets', './bower_components/fontawesome/scss']
	// 				}).on('error', function (err) {
	// 					console.log('SASS ERROR: '+err);
	// 				}))
	// 		)
	// 		.pipe(concat(namespace+'.deps.dev.css')) // <-- don't use concatcss as this will rebase fontawsome urls
	// 		.pipe(gulp.dest('build/www/css'));
	// })
	gulp.task('css-deps', function () {
		return merge(
				gulp.src(mainBowerFiles()
					.concat(manifest.static.css_deps) )
					.pipe(filter('*.css')),
				gulp.src(manifest.static.scss) // <-- note we are including the src (not dep) scss here (I can't remember why)
					.pipe(sass({
						precision: 10,
						includePaths:manifest.static.scss_deps
					}).on('error', function (err) {
						console.error('SASS ERROR: '+err);
					}))
			)
			.pipe(concat(namespace+'.deps.dev.css')) // <-- don't use concatcss as this will rebase fontawsome urls
			.pipe(gulp.dest('build/www/css'));
	})


	gulp.task('js-deps', function () {
		if (debug) return gulp.src(mainBowerFiles().concat(manifest.static.js_deps))
			.pipe(filter('*.js'))
			.pipe(concat(namespace+'.deps.dev.js'))
			.pipe(gulp.dest('build/www/js'));
		else return gulp.src(mainBowerFiles().concat(manifest.static.js_deps))
			.pipe(filter('*.js'))
			.pipe(uglify())
			.pipe(concat(namespace+'.deps.dev.js'))
			.pipe(gulp.dest('build/www/js'));
	})

	gulp.task('js', ['browserify'], function () {
		return gulp.src(manifest.static.js)
			.pipe(concat(namespace+'.dev.js'))
			.pipe(gulp.dest('build/www/js'))
			.on('error', function(err) {
				console.error(err);
			})
	})

	gulp.task('css', function() {
		return gulp.src(manifest.static.css)
			.pipe(filter('*.css'))
			.pipe(concat(namespace+'.dev.css'))
			.pipe(gulp.dest('build/www/css'));
	})

	// gulp.task('html-dev', function () {
	// 	console.log('html-dev: only processes src/html/index.dev.html - uses gulp-html-replace to remove content security policy')
	// 	return gulp.src('src/html/index.dev.html') // <-- TODO : use manifest.static.html (need to update templates)
	// 		.pipe(require('gulp-html-replace')({
	// 			'cors':''
	// 		}))
	// 		.pipe(rename('index.html'))
	// 		.pipe(gulp.dest('build/www'));
	// })

	// gulp.task('html-dev-cors', function () {
	// 	console.log('html-dev: only processes src/html/index.dev.html - uses gulp-html-replace to merge content security policy (referencing cors.html which I can\'t see in any project')
	// 	var cors = fs.readFileSync('src/html/cors.html', 'utf8');
	// 	return gulp.src('src/html/index.dev.html')
	// 		.pipe(require('gulp-html-replace')({
	// 			'cors': cors
	// 		}))
	// 		.pipe(rename('index.html'))
	// 		.pipe(gulp.dest('build/www'));
	// })

	gulp.task('server', function () {
		return gulp.src(manifest.server)
			.pipe(gulp.dest('build'));
	})
}