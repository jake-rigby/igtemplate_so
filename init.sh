#!/bin/sh
if [ -d "../$1" ]; then
  # Control will enter here if directory exists.
  exit "$1 already exists"
fi
  git clone ./ "../$1"
  (
  	cd "../$1"
  	rm -rf .git
  	sudo npm install
  	bower install
  	gulp init --name "$1"
  )
