
var	del = require('del'),
	replace = require('gulp-replace'),
	runsequence = require('run-sequence'),
	configpath = './config.json',
	config = require(configpath),
	flatten = require('flat'),
	jeditor = require('gulp-json-editor'),
	manifest = require('./manifest'),
	rename = require('gulp-rename');

var gulp = require('gulp-param')(require('gulp'), process.argv);

require('./lib/gulp-tasks')(gulp, manifest, config.ns, config.debug);


gulp.task('clean', function (cb) {
	del(['build', 'browserified'], cb);
})


gulp.task('build',[
	'browserify',
	'server',
	'js',
	'js-deps',
	'css',
	'css-deps',
	'fonts',
	'images',
	'images-deps',
	'html',
	'html-deps',
	'server'
]);


function replaceConfigs (stream) {
	var replaceParams = flatten(config);
	// console.log('replacing:',replaceParams);
	for (var rp in replaceParams) {
		var op = replace('$igtemplate-'+rp+'|', replaceParams[rp]);
		stream = stream.pipe(op);
	}
	return stream;
}


gulp.task('files', function () {
	return replaceConfigs(gulp.src(['build/**/*', ,'!build/www/fonts/**/*', '!build/www/images']))
		.pipe(gulp.dest('dist'));
})


// remove deDeps and copy package.json to dist so it can be installed with npm elswhere
gulp.task('package-json', function () {
	return gulp.src('package.json')
		.pipe(jeditor({'devDependencies': null}))
		.pipe(jeditor(function (json) { // so we can have private bitbucket dependencies in openshift (see .openshift/action_hooks/deploy)
			for (var dep in config.remove_bitbucket_private) {
				delete json.dependencies[config.remove_bitbucket_private[dep]];
			}
			return json
		}))
		.pipe(gulp.dest('dist'))
})

// rename in case we are using a config-debug.json or similar
gulp.task('copy-config', function () {
	return gulp.src(configpath)
		.pipe(rename({
			basename: "config",
			extname: ".json"
		}))
		.pipe(gulp.dest('dist'))
})

// openshift configs
gulp.task('openshift', function () {
	return gulp.src('./.openshift/**/*')
		.pipe(gulp.dest('dist/.openshift'))
})

// takes the config path through shell args
gulp.task('main', function (path) {
	if (typeof path === 'string') {
		config = require(path);
		configpath = path;
	}
	runsequence('clean', 'build', 'files'/*, 'image-files'*/, 'package-json', 'copy-config'/*, 'openshift'*/);
})

gulp.task('init', function (name) {
	
	// console.log('init',name);
	
	gulp.src(['./README.md', './bower.json', './config-local.json', './config.json'/*, './gulpfile.js'*/, './init.sh', './lib', './manifest.json', './package.json', './src', './test.sh'])
		.pipe(replace('igtemplate', name))
		.pipe(gulp.dest('./'))
		
	gulp.src('./package.json')
		.pipe(replace('igtemplate_so', name))
		.pipe(gulp.dest('./'))
})

